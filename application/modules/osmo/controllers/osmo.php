<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class osmo extends MX_Controller {

  function __construct() {
    parent::__construct();
  $this->load->model('mdl_osmo', 'mdl_osmo');
  $this->load->library('session');
   
  
}

 function index()
      {
        $this->mdl_osmo->tablename='portfolio';
        $data['num'] = $this->mdl_osmo->count_all();
        if ($data['num'] < 8) {
          $allWork = $this->mdl_osmo->get('id');
          $data['allWork'] = $allWork;
        }else{
          $offset = $data['num'] - 8 ;
          $limit = 8;
          $allWork = $this->mdl_osmo->get_with_limit($limit, $offset, 'id'); 
          $data['allWork'] = $allWork;
        }
        
        $this->load->view('index',$data);

        
      }


      function pastwork()
      {
          $this->load->model('mdl_osmo');
        $this->mdl_osmo->tablename='work';
        $data['num'] = $this->mdl_osmo->count_all();
        $offset = $data['num'] - 4 ;
        $limit = 4;
          $allWork = $this->mdl_osmo->get_with_limit($limit, $offset, 'id'); 
          $data['allWork'] = $allWork;
          $this->load->view('pastwork', $data);

        
      }


      function contact()
      {
        $this->load->view('contact');
      }

      function services()
      {
        $this->load->view('services');
      }




      function postinquiry(){
              $this->load->model('mdl_osmo');
              $this->mdl_osmo->tablename='inqiury';
              $data = $this->get_form_data();
              $this->mdl_osmo->_insert($data);
              redirect(base_url('osmo/contact'));
              echo '
                <script type="text/javascript">
                alert();
                  </script> 
              ' ;
      }



      function about()
      {
        $this->load->view('about');
      }
      function home()
      {
          $this->load->library('session');  
          $this->load->model('mdl_users');
          $this->mdl_products->tablename='users';
          $data['query']=$this->mdl_users->get('id');
        $this->load->view('home',$data);
      }
      function _in_you_go($username){
          // give them a session variable and sent them to the admin panel
          $this->load->model('mdl_users');
          $this->mdl_users->tablename='users';
          $query= $this->mdl_users-> get_where_custom('username',$username);
          foreach ($query -> result() as $row){
            $user_id = $row->id;
            $Username = $row->username;
            $email = $row->email;
          }
          
          $this->session->set_userdata('user_id', $user_id);
          $this->session->set_userdata('username',$username);
          $this->session->set_userdata('email',$email);
          redirect (base_url ('users/home'));
        }

      function registeruser(){
         $this->load->library('form_validation');

                $this->form_validation->set_rules('username', 'Username','required|max_length[30]|xss_clean');
                $this->form_validation->set_rules('password', 'Password', 'required|max_length[30]|xss_clean');
           $this->form_validation->set_rules('email', 'Email', 'required|max_length[30]|xss_clean');
  
                  if ($this->form_validation->run($this) == FALSE)
          {
            $this->index();
          }
          else{
              $this->load->model('mdl_users');
              $this->mdl_users->tablename='users';
              $data= $this->get_form_data();
              $p = $data['password'];
              $data['password'] = modules:: run('site_security/awesome_unbreakable_super_mega_hash',$p);
              $this->mdl_users->_insert($data);
              redirect('users/index');


          }

        
      }

      function login(){
           $this->load->library('form_validation');

                $this->form_validation->set_rules('username', 'Username','required|max_length[30]|xss_clean');
                $this->form_validation->set_rules('password', 'Password', 'required|max_length[30]|xss_clean|callback_pword_check');
                

                if ($this->form_validation->run($this) == FALSE)
                {
                        $this->index();
                }
                else
                {
                 $username = $this->input->post('username',TRUE);
                 $this->_in_you_go ($username); 
                 
                }
        }
       function pword_check($password) {
                $username = $this->input->post('username',TRUE);
                //the next line helps remove the encryption property
                $password = modules::run('site_security/make_hash', $password);
                $this->load->model('mdl_users');
                $this->mdl_users->tablename='users';
                // collect username and password and send them to fuuction pword_check in mdl_user
                $result = $this->mdl_users->pword_check($username, $password);

                      if ($result == FALSE)
                      {
                        // ensure u add form validation php file to libraries and run this
                        // or u can search for hmvc call back on youtube 
                              $this->form_validation->set_message('pword_check', 'You did not enter correct username and/or password ');
                              return FALSE;
                      }
                      else
                      {
                              return TRUE;
                      }
              }


    

      function get_form_data(){
          $data = $this->input->post();
          return $data;
      }

       function logout(){
             $this->load->library('session');
            $this->session->sess_destroy();
            redirect('users/index');
          }

    
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

?>
