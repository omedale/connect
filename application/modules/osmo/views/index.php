<!DOCTYPE html>
 <html class="">
    <head>
        <meta charset="utf-8">
        <title>Osmotech Solutions - Nigeria Website Development, Mobile App Development, SEO, Ecomerce company</title>
    	<meta name="description" content="Osmotech solutions helps clients to build responsive web app , mobile app and we also do search engine optimization ">
        <meta name="viewport" content="width=device-width">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/templatemo_misc.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/templatemo_style.css">
         <link rel="stylesheet" href="<?php echo base_url();?>assets/css/myscript.css">

        <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="site-main" id="sTop">
            <div class="site-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <ul class="social-icons">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-dribbble"></a></li>
                                <li><a href="#" class="fa fa-linkedin"></a></li>
                            </ul>
                        </div> <!-- /.col-md-12 -->
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
                <div class="main-header">
                    <div class="container">
                        <div id="menu-wrapper">
                            <div class="row">
                                <div class="logo-wrapper col-md-2 col-sm-2">
                                    <h1>
                                        <a href="#">Osmotech</a>
                                    </h1>
                                </div> <!-- /.logo-wrapper -->
                                <div class="col-md-10 col-sm-10 main-menu text-right">
                                    <div class="toggle-menu visible-sm visible-xs"><i class="fa fa-bars"></i></div>
                                    <ul class="menu-first">
                                        <li class="active" style="position: relative;"><a href="#">Home</a></li>
                                        <li><a href="#services" style="position: relative;">Services</a></li>
                                        <li><a href="#portfolio" style="position: relative;">Portfolio</a></li>
                                        <li><a href="#our-team" style="position: relative;">Team</a></li>
                                       <li><a href="#contact" style="position: relative;">About</a></li>
                                                                       
                                    </ul>                                    
                                </div> <!-- /.main-menu -->
                            </div> <!-- /.row -->
                        </div> <!-- /#menu-wrapper -->                        
                    </div> <!-- /.container -->
                </div> <!-- /.main-header -->
            </div> <!-- /.site-header -->
            <div class="site-slider">
                <div class="slider">
                    <div class="flexslider">
                        <ul class="slides">

                            <li>
                                <div class="overlay"></div>
                                <img src="<?php echo base_url();?>assets/images/slide1.jpg" alt="">
                                <div class="slider-caption visible-md visible-lg">
                                    <h2>Mobile Apps</h2>
                                    <p>We make beautifully designed with technical excellence and exceptional attention to detail.</p>
                                     <a href="#portfolio" class="slider-btn">Go to Portfolio</a>
                                </div>
                            </li>
                            <li>
                                <div class="overlay"></div>
                                <img src="<?php echo base_url();?>assets/images/slide3.jpg" alt="">
                                <div class="slider-caption visible-md visible-lg">
                                    <h2>Responsive web applications</h2>
                                    <p>We deliver unparalleled web development solutions to fit each of our customers.</p>
                                    <a href="#portfolio" class="slider-btn">Go to Portfolio</a>
                                </div>
                            </li>
                            <li>
                                <div class="overlay"></div>
                                <img src="<?php echo base_url();?>assets/images/slide2.jpg" alt="">
                                <div class="slider-caption visible-md visible-lg">
                                    <h2>Our Work</h2>
                                    <p>We ensure client satisfaction at all time and we provide 24 hours technical support</p>
                                    <a href="#portfolio" class="slider-btn">Go to Portfolio</a>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- /.flexslider -->
                </div> <!-- /.slider -->
            </div> <!-- /.site-slider -->
        </div> <!-- /.site-main -->


        <div class="content-section" id="services">
            <div class="container">
                <div class="row">
                    <div class="heading-section col-md-12 text-center">
                        <h2>Our Services</h2>
                        <p style="text-transform: none;">Obiquitous computing has made life easier; this concept has grown faster in recent time with the advent of cloud computing, this in turn made the use of mobile phone and website imperative for individuals and organisations. Let us help you make your business and daily activity pervasive !!</p>
                    </div> <!-- /.heading-section -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-1">
                            <div class="service-icon">
                            <h3 class="bod">Responsive Web Apps</h3>
                                <i class="fa fa-file-code-o"></i>
                            </div> <!-- /.service-icon -->
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>Responsive Web Apps</h3>
                                   <p>We deliver unparalleled web development solutions to fit each of our clients and ensure it delivers good user experience. </p> 
                                </div>
                            </div> <!-- /.service-content -->
                        </div> <!-- /#service-1 -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-2">
                            <div class="service-icon">
                             <h3 class="bod">Mobile Applications</h3>
                                <i class="fa fa-paper-plane-o"></i>
                            </div> <!-- /.service-icon -->
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>Mobile & Desktop Applications</h3>
                                   <p>At Osmotech Solutions we do extensive research to assure we cater for your users’ devices and operation systems to allow for business to remain uninterrupted.</p> 
                                </div>
                            </div> <!-- /.service-content -->
                        </div> <!-- /#service-1 -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-3">
                            <div class="service-icon ">
                            <h3 class="bod">SEO</h3>
                                <i class="fa fa-search"></i>
                            </div> <!-- /.service-icon -->
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>SEO</h3>
                                   <p>Get immediate increases to traffic, visibility on all search engines, traffic from customers looking for your product & more sales leads.</p> 
                                </div>
                            </div> <!-- /.service-content -->
                        </div> <!-- /#service-1 -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-4">
                            <div class="service-icon">
                            <h3 class="bod">Ecommerce Solutions</h3>
                                <i class="fa fa-shopping-cart"></i>
                            </div> <!-- /.service-icon -->
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>Ecommerce Solutions</h3>
                                   <p>Every business sells something, be it services or products. Osmotech Solutions assist you in building your e-commerce business from design to development and allow you sell to a world of potential customers 24/7.</p> 
                                </div>
                            </div> <!-- /.service-content -->
                        </div> <!-- /#service-1 -->
                    </div> <!-- /.col-md-3 -->
                </div> <!-- /.row -->
                 
            </div> <!-- /.container -->
        </div> <!-- /#services -->



        <div class="content-section" id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="heading-section col-md-12 text-center">
                        <h2>Our Portfolio</h2>
                        <p>What we have done so far</p>
                    </div> <!-- /.heading-section -->
                </div> <!-- /.row -->
                <div class="row">
                <?php
                     $res = array_reverse($allWork->result());
                    $index = 0;
                    foreach ($res as  $value) {
                        echo '
                            <div class="portfolio-item col-md-3 col-sm-6">
                        <div class="portfolio-thumb">
                            <img src="'.base_url('pictures/work/')."/".$value->image.'" alt="">
                            <div class="portfolio-overlay">
                                <h3>'.$value->title.'</h3>
                                <p>'.$value->description.'.</p>
                                <a href="https://'.$value->link.'"  target="_blank" class="expand ico">
                                    <i class="fa fa-link"></i>
                                </a>
                                <a href="'.base_url('pictures/work/')."/".$value->image.'" data-rel="lightbox" class="expand ico" style="margin: 2px;">
                                    <i class="fa fa-image"></i>
                                </a>
                            </div> <!-- /.portfolio-overlay -->
                        </div> <!-- /.portfolio-thumb -->
                    </div> <!-- /.portfolio-item -->
                        ';
                        $index+=1;
                    }

                 ?>
                   
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /#portfolio -->




        <div class="content-section" id="our-team">
            <div class="container">
                <div class="row">
                    <div class="heading-section col-md-12 text-center">
                        <h2>Our Team</h2>
                        
                        <p style="    text-transform: inherit;">We are a motivated group of trained, talented and dedicated professionals; We are a passionate team of vast expertise that are dedicated to helping you bring your ideas to market and to the international stage, we place our clients’ need and expectations above and beyond everything else, while maintaining diligence and integrity in all our dealings.</p>
                    </div> <!-- /.heading-section -->
                </div> <!-- /.row -->
               
            </div> <!-- /.container -->
        </div> <!-- /#our-team -->




        <div class="" id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                     <div style="margin-bottom: 15px; margin-top: 15px;" class="heading-section col-md-12 text-center">
                        <h2>About Us</h2>
                        
                    </div> 
                        <p>We are a software and web application development service provider with clear focus on high quality, time beating and cost-effective business automation products and services. With our experience in providing high quality software and implement/development services, capabilities and standards, it makes us to develop solutions that give businesses a cutting edge operation.
                        <br/>
                        We have a passionate team of digital experts that are dedicated to helping you bring your ideas to market and also help you to boost your internet presence. Our wealth of experience allow us to offer you creative work and the best in class technology. We also utilize our best practices to save you time and money without sacrificing quality.
                    	</p>
                        <p style="font-size: 20px; border-bottom: 1px solid #444444; display: inline-block;">Our Vision</p>
                        <p>To be one of Nigeria’s leading ICT organization by providing solutions to problems facing human wants, providing solutions to problems with the aid of ICT recognized globally.</p>
                         <p style="font-size: 20px; border-bottom: 1px solid #444444; display: inline-block;">Our Mission</p>
                        <p>To use ICT to solve man problems while respecting man dignity and privacy.</p>
                        <ul class="contact-info">
                            <li>Phone: +2347033390748</li>
                            <li>Email: <a href="">omedale@gmail.com</a></li>
                            <li>Address:Oba Elegushi Street, Ikoyi Lagos.</li>
                        </ul>
                        <!-- spacing for mobile viewing --><br><br>
                    </div> <!-- /.col-md-7 -->
                    <div class="col-md-5 col-sm-6">
                     <div style="margin-bottom: 15px; margin-top: 15px;" class="heading-section col-md-12 text-center">
                        <h2>Contact Us</h2>
                        <p>Feel free to send a message</p>
                    </div> <!-- /.heading-section -->
                        <div class="contact-form">
                            <form  name="contactform"  id="contactform">
                                <p>
                                    <input name="name" type="text" id="name" placeholder="Your Name" required>
                                </p>
                                <p>
                                    <input name="email" type="email" id="email" placeholder="Your Email" required> 
                                </p>
                                <p>
                                    <input name="subject" type="text" id="subject" placeholder="Subject" required> 
                                </p>
                                <p>
                                    <textarea name="message" id="message" placeholder="Message"></textarea required>    
                                </p>
                                <input type="button" class="mainBtn" onclick="sendMessage()"  id="submit" value="Send Message" required>
                            </form>
                        </div> <!-- /.contact-form -->
                    </div> <!-- /.col-md-5 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /#contact -->
            
        <div class="row  client-logo">
                        <div class="col-sm-12 col-md-12 client-logo">
                            <ul class="list-unstyled list-inline text-center">
                                <li><img src="<?php echo base_url(); ?>assets/images/client-logo/3sponsor.jpg" alt="client name"></li>
                                <li><img src="<?php echo base_url(); ?>assets/images/client-logo/6sponsor.jpg" alt="client name"></li>
                                <li><img src="<?php echo base_url(); ?>assets/images/client-logo/5sponsor.jpg" alt="client name"></li>
                                
                                </ul>
                        </div>
                </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog" style="max-width: 300px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Delivery Status</h3>
                </div>
                <div class="modal-body">
                    <p>Message Sent</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog" style="max-width: 300px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Note:</h3>
                </div>
                <div class="modal-body">
                    <p>All fields are required</p>
                </div>
            </div>
        </div>
    </div>


        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-xs-12 text-left">
                        <span>Copyright &copy; 2015 Osmotech Solutions 
                        
                        <!--
                        - Designed by <a rel="nofollow" href="http://www.templatemo.com/tm-406-flex" target="_parent">Flex Template</a>
                        -->
                        </span>
                  </div> <!-- /.text-center -->
                    <div class="col-md-4 hidden-xs text-right">
                        <a href="#top" id="go-top">Back to top</a>
                    </div> <!-- /.text-center -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /#footer -->
        
        <script src="<?php echo base_url();?>assets/js/jquery-1.11.2.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url();?>assets/js/main.js"></script>
        <script src="<?php echo base_url();?>assets/js/app.js"></script>

        <!-- Google Map -->
       
        <!-- templatemo 406 flex -->
    </body>
</html>