<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>Osmotech</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="bs-css" href=" <?php echo base_url(); ?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href=" <?php echo base_url(); ?>assets/css/charisma-app.css" rel="stylesheet">
    
    <link href=' <?php echo base_url(); ?>assets/css/bootstrap-darkly.min.css' rel='stylesheet'>

    

    <!-- jQuery -->
    

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
 

</head>

<body>
<div class="ch-container">
    <div class="row">
        
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Welcome to Osmotech</h2>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
         <?php 
         if(isset($errormsg)){
            echo $errormsg;
         }else{
            echo "<div class='alert alert-info'>  Please login with your Username and Password.</div>";
         }
           ?>
            
            <form class="form-horizontal" action="<?php echo base_url('admin/login'); ?>" method="post">
                <fieldset>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                        <input type="text" name="email" id="exampleInputEmail1" class="form-control" placeholder="email">
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class=" glyphicon glyphicon-off red"></i></span>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="clearfix"></div>

                    <div class="input-prepend">
                        <label class="remember" for="remember"><input type="checkbox" id="remember"> Remember me</label>
                    </div>
                    <div class="clearfix"></div>

                    <p class="center col-md-5">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </p>
                </fieldset>
            </form>
        </div>
        <!--/span-->
    </div><!--/row-->
</div><!--/fluid-row-->

</div><!--/.fluid-container-->

<!-- external javascript -->
<script src=" <?php echo base_url(); ?>assets/bower_components/jquery/jquery.min.js"></script>
<script src=" <?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



</body>
</html>
