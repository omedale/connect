<?php 
    $sesiondata=$this->session->all_userdata();
   
   ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>gsffuta felloship</title>
    
    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('assets/favicon/apple-touch-icon-57x57.png');?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/favicon/apple-touch-icon-60x60.png');?>">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/favicon/favicon-16x16.png');?>" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/favicon/favicon-32x32.png');?>" sizes="32x32">
    <meta name="msapplication-TileColor" content="#da532c">
    
    <!--Bootstrap and Other Vendors-->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-theme.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/owl.carousel/css/owl.carousel.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendors/rs-plugin/css/settings.css');?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendors/js-flickr-gallery/css/js-flickr-gallery.css');?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendors/lightbox/css/lightbox.css');?>" media="screen" />
    
    <!--Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <!--Construction Styles-->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
    
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="pageloader" class="row m0">
        <div class="loader-item"><img src="<?php echo base_url('assets/images/loader.gif');?>" alt="loading"></div>
    </div> 
    <section id="nr_topStrip" class="row">
        <div class="container">
            <div class="row">
                <ul class="list-inline c-info fleft">
                    <li><a href="tel:123456789012"><i class="fa fa-phone"></i>+234-703-355-6109 or +234-802-397-0193</a></li>
                    <li><a href="mailto:info@domain.com"><i class="fa fa-envelope-o"></i> info@gsffuta.com</a></li>
                     <?php 
                        if(isset($sesiondata['username'])){
                            echo ' <li><a href=""><i class="fa fa-user">Hi '.$sesiondata['username'].'</i></a></li>';
                        }
                    ?>
                    <li><a href="<?php echo base_url('gsffuta/logout');?>"><i class="fa fa-power-off"></i>logout</a></li>
                </ul>
               
            </div>
        </div>
    </section> <!--Top Strip-->
    
    <header class="row">
        <div class="container">
            <div class="row">
                <div class="logo col-sm-6">
                    <div class="row">
                        <a href="#"><img src="<?php echo base_url('assets/images/logo.jpg');?>" alt="Gsffuta scholarship website"></a>
                    </div>
                </div>
                <div class="social_nav col-sm-6">
                    <div class="row">
                        <ul class="list-inline fright">
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header> <!--Header-->
    
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid container">
            <div class="row m04m">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_nav">
                         
                        <span class="btn-text">Select Page</span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="nav navbar-nav">
                        <li class="active "><a href="#">Home</a></li>
                       
                           
                        <li class=" dropdown">
                        <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About us</a>
                          <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo base_url('gsffuta/about');?>">GSF</a></li>
                                <li><a href="<?php echo base_url('gsffuta/academics');?>">Academics</a></li>
                                <li><a href="<?php echo base_url('gsffuta/kpt');?>">KPT Crew</a></li>
                            </ul>
                        </li>

                      
                         <li><a href="<?php echo base_url('gsffuta/news');?>">News</a></li> 
                        <li><a href="<?php echo base_url('gsffuta/scholarships');?>">Scholarships</a></li>  
                        <li><a href="<?php echo base_url('gsffuta/contacts');?>">Contact us</a></li> 
                       
                    </ul>
                </div>
            </div>
        </div>
    </nav> <!--Main Nav-->
    
      <section id="nr_slider" class="row">
        <div class="mainSliderContainer">
            <div class="mainSlider" >
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="boxslide" data-slotamount="7" >
                       <img src="<?php echo base_url('assets/images/slider/everlove.jpg');?>" alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                       <div class="caption sfr str"  
                           data-x="center" 
                           data-y="140" 
                           data-speed="700"  
                           data-start="1700" 
                           data-easing="easeOutBack">
                            <h4 style="color:#f7b71e;">We Are God's Sweet Family</h4>
                       </div>
                       <div class="caption sfl stl"  
                           data-x="center" 
                           data-y="225" 
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack">
                            <div class="cont-row">from <span class="bb3">small groups</span> to <span class="bb1">Royal generations</span></div>
                       </div>
                       <div class="caption skewfromleft skewtoleft"  
                           data-x="center" 
                           data-y="310"
                           data-hoffset="-176" 
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack"><div class="ico_box"><img src="<?php echo base_url('assets/images/slider/ico1.png');?>" alt="brifcase"></div>
                       </div>
                       <div class="caption sfb stb"  
                           data-x="center" 
                           data-y="310" 
                           data-hoffset="0"
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack"><div class="ico_box"><img src="<?php echo base_url('assets/images/slider/ico2.png');?>" alt="brifcase"></div>
                       </div>
                       <div class="caption skewfromright skewtoright"  
                           data-x="center" 
                           data-y="310" 
                           data-hoffset="176"
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack"><div class="ico_box"><img src="<?php echo base_url('assets/images/slider/ico3.png');?>" alt="brifcase"></div>
                       </div>                       
                    </li> 
                    <li data-transition="boxslide" data-slotamount="7" >
                       <img src="<?php echo base_url('assets/images/slider/slide1.jpg');?>" alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                       <div class="caption sfr str"  
                           data-x="center" 
                           data-y="140" 
                           data-speed="700"  
                           data-start="1700" 
                           data-easing="easeOutBack">
                            <h2 style="color:#f7b71e;">We Are <strong>Role </strong>Models</h2>
                       </div>
                       <div class="caption sfl stl"  
                           data-x="center" 
                           data-y="225" 
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack">
                            <div class="cont-row">from <span class="bb3">small ideas</span> to <span class="bb1">big dreams</span></div>
                       </div>
                       <div class="caption skewfromleft skewtoleft"  
                           data-x="center" 
                           data-y="310"
                           data-hoffset="-176" 
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack"><div class="ico_box"><img src="<?php echo base_url('assets/images/slider/ico1.png');?>" alt="brifcase"></div>
                       </div>
                       <div class="caption sfb stb"  
                           data-x="center" 
                           data-y="310" 
                           data-hoffset="0"
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack"><div class="ico_box"><img src="<?php echo base_url('assets/images/slider/ico2.png');?>" alt="brifcase"></div>
                       </div>
                       <div class="caption skewfromright skewtoright"  
                           data-x="center" 
                           data-y="310" 
                           data-hoffset="176"
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack"><div class="ico_box"><img src="<?php echo base_url('assets/images/slider/ico3.png');?>" alt="brifcase"></div>
                       </div>                       
                    </li>  
                    <!-- SLIDE 2 -->
                    <li data-transition="boxslide" data-slotamount="7" >
                       <img src="<?php echo base_url('assets/images/slider/slide2.png');?>" alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                       <div class="caption sfr str"  
                           data-x="-100" 
                           data-y="135" 
                           data-speed="700"  
                           data-start="1700" 
                           data-easing="easeOutBack">
                            <h3>Achieving<strong> Academic Excellence </strong> </h3>
                       </div>
                       <div class="caption sfl stl"  
                           data-x="-100" 
                           data-y="190" 
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack">
                            <h4>just got<strong> Easier</strong></h4>
                       </div>
                       <div class="caption skewfromleft skewtoleft"  
                           data-x="-100" 
                           data-y="265"
                           data-hoffset="-176" 
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack">
                           <p>We raise and build role models with big minds -<br>and also equip students with bright ideas in facing a challenging carrier <br>get daily academic tips to make you feel good.</p>
                       </div>
                       <div class="caption sfb stb"  
                           data-x="-100" 
                           data-y="375" 
                           data-hoffset="0"
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack">
                           <a type="button" class="btn btn-default">Learn More</a>
                       </div>
                       <div class="caption skewfromright skewtoright"  
                           data-x="right" 
                           data-y="130" 
                           data-hoffset="176"
                           data-speed="500" 
                           data-start="1900" 
                           data-easing="easeOutBack"><div class="tools"><img src="<?php echo base_url('assets/images/slider/scholarship.png');?>" alt="brifcase"></div>
                       </div>                      
                    </li>  
                    <!-- SLIDE 3 -->
                     
                </ul>
            </div>
        </div>
        
        <div class="container sliderAfterTriangle"></div> <!--Triangle After Slider-->
    </section> <!--Slider-->
    
    <section id="nr_services" class="row">
        <div class="container">
            <div class="row sectionTitles">
                <h2 class="sectionTitle">Our Services</h2>
                <div class="sectionSubTitle">what we offer</div>
            </div>
            <div class="row m0 text-center">
                <div class="col-sm-3">
                    <div class="row m0 service">
                        <div class="row m0 innerRow">
                            <i class="fa fa-laptop"></i>
                            <div class="serviceName" data-hover="Academic Excellence">Academic Excellence</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="row m0 service">
                        <div class="row m0 innerRow">
                            <i class="fa fa-film"></i>
                            <div class="serviceName" data-hover="Raising role models">Raising role models</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="row m0 service">
                        <div class="row m0 innerRow">
                            <i class="fa fa-clock-o"></i>
                            <div class="serviceName" data-hover="Student's Scholarships">Student's Scholarships</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="row m0 service">
                        <div class="row m0 innerRow">
                            <i class="fa fa-building-o"></i>
                            <div class="serviceName" data-hover="Financial Intelligence">Financial Intelligence</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> <!--Services-->
    
    
    <section id="latestPosts" class="row">
        <div class="container">
            <div class="row sectionTitles">
                <h2 class="sectionTitle">God's Sweet Family</h2>
                <div class="sectionSubTitle">little words about us</div>
            </div>
            <div class="row">
                <div class="col-sm-4 blog latest-blog">
                    <div class="row m0 blogInner">
                        <div class="row m0 blogDateTime">
                            <i class="fa fa-calendar"></i> 18 May 2015, 11:50 AM
                        </div>
                        <div class="row m0 featureImg">
                            <a  >
                                <img src="<?php echo base_url('assets/images/silver.jpg');?>" alt="Faceted Search Has Landed" class="img-responsive">
                            </a>
                        </div>
                        <div class="row m0 postExcerpts">
                            <div class="row m0 postExcerptInner">
                                <a href="single-post.html" class="postTitle row m0"><h4>Silver Jubilee</h4></a>
                                <p>Gsf excellent cathedral is very real and the project is going as proposed, th building has gotten to the roof level and not stopping with God's help .</p>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 blog latest-blog">
                    <div class="row m0 blogInner">
                        <div class="row m0 blogDateTime">
                            <i class="fa fa-calendar"></i> 18 May 2015, 11:50 AM
                        </div>
                        <div class="row m0 featureImg">
                            <a  >
                                <img src="<?php echo base_url('assets/images/blog/03.jpg');?>" alt="Faceted Search Has Landed" class="img-responsive">
                            </a>
                        </div>
                        <div class="row m0 postExcerpts">
                            <div class="row m0 postExcerptInner">
                                <a   class="postTitle row m0"><h4>Ecellent Cathedral</h4></a>
                                <p>Gsf excellent cathedral is very real and the project is going as proposed, th building has gotten to the roof level and not stopping with God's help.</p>
                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 blog latest-blog">
                    <div class="row m0 blogInner">
                        <div class="row m0 blogDateTime">
                            <i class="fa fa-calendar"></i> 14 November 2014, 10:50 AM
                        </div>
                        <div class="row m0 featureImg">
                            <a >
                                <img src="<?php echo base_url('assets/images/slider/money.jpg');?>" alt="Faceted Search Has Landed" class="img-responsive">
                            </a>
                        </div>
                        <div class="row m0 postExcerpts">
                            <div class="row m0 postExcerptInner">
                                <a href="#" class="postTitle row m0"><h4>Gsf Futa Scholarship</h4></a>
                                <p>This year in our 2015 roadmap one of our goals is to encourage students striving for academic excellence which will create a platform in awarding scholarships to the less priviledged students .</p>
                                <a href="<?php echo base_url('gsffuta/scholarships');?>" class="readMore">read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> <!--Latest Blog-->
    
    
    
    <section id="elements" class="row">
        <div class="row sectionTitles m0">
            <h2 class="sectionTitle">Worshipping &amp; Repping the Kingdom</h2>
            <div class="sectionSubTitle">making jesus your best pal</div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel-group" id="hAccordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#hAccordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                        <i class="fa fa-question"></i>Have you had your devotion today <span class="sign fa"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="fleft icon">
                                        <i class="fa fa-laptop"></i>
                                    </div>
                                    <div class="fleft texts">
                                       Many new Christians view the Christian life as a long list of “do’s” and “don’ts.” They haven’t yet discovered that spending time with God is a privilege that we get to do not a chore or an obligation that we have to do.
                                    </div>
                                </div>
                            </div>
                        </div> <!--hAccordion No #1--> 
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading2">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#hAccordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                        <i class="fa fa-question"></i>Study to show yourself approved <span class="sign fa"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                <div class="panel-body">
                                    <div class="fleft icon">
                                        <i class="fa fa-laptop"></i>
                                    </div>
                                    <div class="fleft texts">
                                        II Timothy 2:15 admonishes, “Study to show yourself approved unto God, a workman that needs not to be ashamed, rightly dividing the word of truth.”<br/>
                                        The apostle Paul tells us that proper Bible study leads to approval from God. The same verse also speaks of other benefits. First, as workmen, we need not be ashamed—if we study God’s Word. Instead, studying becomes fulfilling and satisfying. You have done what God expects you to do—an acceptable type of self-approval.
                                    </div>
                                </div>
                            </div>
                        </div> <!--hAccordion No #2--> 
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading3">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#hAccordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                        <i class="fa fa-question"></i>Understanding the concept of Faith <span class="sign fa"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                <div class="panel-body">
                                    <div class="fleft icon">
                                        <i class="fa fa-laptop"></i>
                                    </div>
                                    <div class="fleft texts">
                                    Now faith is the substance of things hoped for, the evidence of things not seen. For by it the elders obtained a good report. Heb 11: 1-2 Faith is the principle by which substance is given to hope. This is how we substantiate our hope and as such certain developments
                                    </div>
                                </div>
                            </div>
                        </div> <!--hAccordion No #3--> 
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading4">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#hAccordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                        <i class="fa fa-question"></i>be UNASHAMED for Christ <span class="sign fa"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                <div class="panel-body">
                                    <div class="fleft icon">
                                        <i class="fa fa-laptop"></i>
                                    </div>
                                    <div class="fleft texts">
                                    acquiring a skill today is cheap, because it makes you your own <strong>boss</strong> as we are in the era of jack of all trades, it is better to be a master of one and also have an idea on as much skills as possible.
                                    </div>
                                </div>
                            </div>
                        </div> <!--hAccordion No #4--> 
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading5">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#hAccordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                        <i class="fa fa-question"></i>Watch &amp; Pray <span class="sign fa"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                <div class="panel-body">
                                    <div class="fleft icon">
                                        <i class="fa fa-laptop"></i>
                                    </div>
                                    <div class="fleft texts">
                                    "But keep on the alert at all times, praying that you may have strength to escape all these things that are about to take place, and to stand before the Son of Man.".
                                    </div>
                                </div>
                            </div>
                        </div> <!--hAccordion No #5-->                        
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row m0 leftAlignedTap">
                        <ul class="nav nav-tabs" role="tablist" id="myTab">
                            <li role="presentation" class="active"><a href="#h_tab1" aria-controls="h_tab1" role="tab" data-toggle="tab">
                                <i class="fa fa-laptop"></i> Our Watchword
                            </a></li>
                            <li role="presentation"><a href="#h_tab2" aria-controls="h_tab2" role="tab" data-toggle="tab">
                                <i class="fa fa-briefcase"></i> Today's Devotion
                            </a></li>
                            <li role="presentation"><a href="#h_tab3" aria-controls="h_tab3" role="tab" data-toggle="tab">
                                <i class="fa fa-youtube-play"></i> Worship with us
                            </a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="h_tab1">
                                 <strong>TUESDAY</strong> 6:00pm - 7:30pm:: <i>Bible Study</i><br/><br/>

                                 <strong>FRIDAY</strong> 6:00pm - 7:30pm:: <i>Warfare Service</i><br/><br/>

                                 <strong> SUNDAY</strong> 6:45am - 7:00am:: <i> Workers Prayer</i><br/>
                                 7:00am - 7:45am::<i> Sunday School Service</i><br/>
                                 7:45am - 10:30am::<i> Sunday Service</i><br/><br/>
 
Every First Sunday of the month:: <i>Thanksgiving Service</i><br/>
Every First Friday of the month:: <i>Night of Exploit which starts from 9:00PM</i>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h_tab2">
                                 <strong>TUESDAY</strong> 6:00pm - 7:30pm:: <i>Bible Study</i><br/><br/>

                                 <strong>FRIDAY</strong> 6:00pm - 7:30pm:: <i>Warfare Service</i><br/><br/>

                                 <strong> SUNDAY</strong> 6:45am - 7:00am:: <i> Workers Prayer</i><br/>
                                 7:00am - 7:45am::<i> Sunday School Service</i><br/>
                                 7:45am - 10:30am::<i> Sunday Service</i><br/><br/>
 
Every First Sunday of the month:: <i>Thanksgiving Service</i><br/>
Every First Friday of the month:: <i>Night of Exploit which starts from 9:00PM</i>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="h_tab3">
                                 <strong>TUESDAY</strong> 6:00pm - 7:30pm:: <i>Bible Study</i><br/><br/>

                                 <strong>FRIDAY</strong> 6:00pm - 7:30pm:: <i>Warfare Service</i><br/><br/>

                                 <strong> SUNDAY</strong> 6:45am - 7:00am:: <i> Workers Prayer</i><br/>
                                 7:00am - 7:45am::<i> Sunday School Service</i><br/>
                                 7:45am - 10:30am::<i> Sunday Service</i><br/><br/>
 
Every First Sunday of the month:: <i>Thanksgiving Service</i><br/>
Every First Friday of the month:: <i>Night of Exploit which starts from 9:00PM</i>

 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> <!--Elements-->
    
     <footer id="nr_footer" class="row">
        <div class="container">
            <div class="row goTop">
                <a href="#top"><i class="fa fa-angle-up"></i></a>
            </div>
            <div class="row twitterSlide">                
                <div class="owl-carousel twitterSlider">
                    <div class="item">
                        <i class="fa fa-twitter"></i><br>
                      
                        "When I open a door, no one can close it. And when I close a door, no one can open it. Listen to what I say." Revelation 3:7b (CEV) -  

                    </div>
                    <div class="item">
                        <i class="fa fa-twitter"></i><br>
                        With gsffuta scholarships and academic committee, achieving exploits just got easier
                    </div>
                </div>
            </div>
            <div class="footerWidget row">
                <div class="col-sm-12 widget">
                    <div class="getInTouch_widget row">
                        <div class="widgetHeader row m0"><img src="<?php echo base_url('assets/images/whiteSquare.png');?>" alt="">Get in touch</div>        
                        <div class="row getInTouch_tab m0">
                            <ul class="nav nav-tabs nav-justified" role="tablist" id="getInTouch_tab">
                              <li role="presentation" class="active"><a href="#contactPhone" aria-controls="contactPhone" role="tab" data-toggle="tab"><i class="fa fa-phone"></i></a></li>
                              <li role="presentation"><a href="#contactEmail" aria-controls="contactEmail" role="tab" data-toggle="tab"><i class="fa fa-envelope"></i></a></li>
                              <li role="presentation"><a href="#contactHome" aria-controls="contactHome" role="tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                            </ul>

                            <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="contactPhone"><i class="fa fa-phone"></i> call us at: +2347033556109/ +2347030253949/ +2348148728642</div>
                              <div role="tabpanel" class="tab-pane" id="contactEmail"><i class="fa fa-envelope"></i>email us at: gsfscholarship@gmail.com</div>
                              <div role="tabpanel" class="tab-pane" id="contactHome"><i class="fa fa-home"></i>Street P, Road 6, Excellent Cathedral, Futa south gate Akure, Ondo State</div>
                            </div>
                        </div>                        
                    </div>
                </div>
                
            </div>
            <div class="row copyrightRow">
                &copy; 2015 <a href="#">Gsffuta</a>, All Rights Reserved
            </div>
        </div>
    </footer>
    
    
    <!--jQuery, Bootstrap and other vendor JS-->
   <script src="<?php echo base_url('assets/js/jquery-2.1.3.min.js');?>"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    
    <script src="<?php echo base_url('assets/vendors/rs-plugin/js/jquery.themepunch.tools.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/rs-plugin/js/jquery.themepunch.revolution.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/owl.carousel/js/owl.carousel.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/nicescroll/jquery.nicescroll.js');?>"></script>
    
    <script src="<?php echo base_url('assets/vendors/js-flickr-gallery/js/js-flickr-gallery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/lightbox/js/lightbox.min.js');?>"></script>
    <!--Isotope-->
    <script src="<?php echo base_url('assets/vendors/isotope/isotope-custom.js');?>"></script>
    
    <!--Construction JS-->
    <script src="<?php echo base_url('assets/js/construction.js');?>"></script>
</body>

</html>