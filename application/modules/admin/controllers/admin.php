<?php header("Access-Control-Allow-Origin: *");

require_once('pusher.php');

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends MX_Controller {

 function index()
      {   
       
        $this->load->library('session');
          $this->load->view('login');
      }

      function showAdminuser(){
         $this->load->library('session');
          if ($this->session->userdata('logged')){
            $this->load->model('mdl_admin');      
          $allAdminUsers = $this->mdl_admin->get_all_users('id'); 
          $data['allAdminUsers'] = $allAdminUsers;
          $this->load->view('adminuser', $data);
        }else{
          redirect(base_url('admin/login'));
        }
      }

      function showPortfolio(){
        $this->load->library('session');
          if ($this->session->userdata('logged')){
            $this->load->model('mdl_admin');  
            $this->mdl_admin->tablename='portfolio';      
          $allPortfolio = $this->mdl_admin->get('id');    
          $data['allPortfolio'] = $allPortfolio;
          $this->load->view('portfolio', $data);
        }else{
          redirect(base_url('admin/login'));
        }
      }

            function showInquiry(){
        $this->load->library('session');
          if ($this->session->userdata('logged')){
            $this->load->model('mdl_admin');
            $this->mdl_admin->tablename='inquiry';      
          $allInqiury = $this->mdl_admin->get('id'); 
          $data['allInqiury'] = $allInqiury;
          $this->load->view('inqiury', $data);
        }else{
          redirect(base_url('admin/login'));
        }
      }

      function deleteinquiry(){
      $this->load->model('mdl_admin');
            $this->mdl_admin->tablename='inquiry';
            $filter =$this->uri->segment(3);
            $query=$this->mdl_admin->get('id');
            $this->mdl_admin->_delete($filter);
            redirect(base_url('admin/showInquiry'));

      }

      function sendMessage(){

        $this->load->library('session');
        $this->load->model('mdl_admin');
        $this->mdl_admin->tablename='inquiry';
        $data=$this->get_form_data();
        $this->mdl_admin->_insert($data);
        print_r(json_encode($query->result()));
      }

      function login(){
        $this->load->library('form_validation');

                $this->form_validation->set_rules('email', 'Email','required|max_length[50]|xss_clean');
                $this->form_validation->set_rules('password', 'Password', 'required|max_length[30]|xss_clean|callback_pword_check');
                

                if ($this->form_validation->run($this) == FALSE)
                {
                       
                        $data['errormsg'] = validation_errors("<div style='color:red;' class='alert alert-info'> " ," </div>");
                     $this->showLogin( $data);
                }
                else
                {   
                  $this->load->model('mdl_admin');
                  
                        $data = $this->get_form_data();
               $this->mdl_admin->tablename='adminusers';
               $query=$this->mdl_admin->get('id');
              $bool=true;
            
            foreach ($query->result() as $key => $value) {
                  
              if (($data['email']==$value->email && $data['password']==$value->password )){
               $this->_in_you_go ($data['email']); 
               $bool=false;
            }}
            if($bool) {
                $errormsg='<div style="color:red;"" class="alert alert-info"> Invalid user </div>';
                $data['errormsg']=$errormsg;
                $this->go($errormsg);
               // redirect(base_url('admin'),$data);}
              //$this->load->view('login',$data);}
               }
             
      }
    }

     function go($errormsg)
      {   
        if ($errormsg == ''){
           $this->load->library('session');
          $this->load->view('login');
        }else{
          $data['errormsg'] = $errormsg;
           $this->load->view('login', $data);
        }
       
      }

      function work(){
        $this->load->library('session');
        if ($this->session->userdata('logged')){
          $this->load->model('mdl_admin');
        $this->mdl_admin->tablename='work';
          $allWork = $this->mdl_admin->get('id'); 
          $data['allWork'] = $allWork;
          $this->load->view('work', $data);
        }else{
          redirect(base_url('admin/login'));
        }
        
      }

        function inqiury(){
          $this->load->library('session');
          if ($this->session->userdata('logged')){
                $this->load->model('mdl_admin');
        $this->mdl_admin->tablename='inqiury';
          $allInqiury = $this->mdl_admin->get('id'); 
          $data['allInqiury'] = $allInqiury;
          $this->load->view('inqiury', $data);
        }else{
          redirect(base_url('admin/login'));
        }
      
      }

      function editUser(){
        $this->load->library('session');
        $this->load->model('mdl_admin');
         $this->mdl_admin->tablename='adminusers';
        $filter = $this->uri->segment(3);
        $data=$this->get_form_data();
        $post = $this->input->post('post',TRUE);
          $query=$this->mdl_admin->get_where_custom('id', $filter);
    
                  $id=0;
                  foreach ($query->result() as  $value) {
                      $id=$value->id;
                      $img=$value->picture;
                  }

             if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != '') {
                  $config['upload_path'] = './pictures/admin/';
                  $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
                  $config['file_name'] = $id.'pics.jpg';
                  $config['overwrite']=true;
                  $this->load->library('upload', $config);
                 
                       if ( ! $this->upload->do_upload())
                  {
                  echo 'error uploading file';
                  }else{
                      
                          $data['picture']=$id.'pics_thumb.jpg';
                          $file_name = $id.'pics.jpg';
                          $filePath = './pictures/admin/';
                          $width = 440;
                          $height = 390;
                          $mainfolder ='pictures/admin';
                          $this->resize_image($mainfolder,$filePath,$file_name, $width, $height,$id);
                  }
                }else{
                      $data['picture']=$img;
                }   

       
           //   $p = $data['password'];
            //  $data['password'] = modules:: run('site_security/awesome_unbreakable_super_mega_hash',$p);
                 
              $this->mdl_admin->_update($filter,$data);
              redirect(base_url('admin/showAdminuser'));



      }

         function editPortfolio(){
          $this->load->library('session');
          $this->load->model('mdl_admin');
          $this->mdl_admin->tablename='portfolio';
        $filter = $this->uri->segment(3);
        $data=$this->get_form_data();
          $query=$this->mdl_admin->get_where_custom('id', $filter);
    
                  $id=0;
                  foreach ($query->result() as  $value) {
                      $id=$value->id;
                      $img=$value->image;
                  }

                 

                  $config['upload_path'] = './pictures/work/';
                  $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
                  $config['file_name'] = $id.'pics.jpg';
                  $config['overwrite']=true;
                  $this->load->library('upload', $config);
                  if($config['file_name']==''){
                      $data['image']=$img;
                  }else{
                       if ( ! $this->upload->do_upload())
                  {
                  echo 'error uploading file';
                  }else{
                      
                          $data['image']=$id.'pics.jpg';
                  }
                  }

                  

                 
              $this->mdl_admin->_update($filter,$data);
              redirect(base_url('admin/showPortfolio'));


        }

      function savePortfolio(){
              $this->load->library('session');
              $this->load->model('mdl_admin');
              $this->mdl_admin->tablename='portfolio';
              $data= $this->get_form_data();
               $query=$this->mdl_admin->get('id');
    
                  $id=0;
                  foreach ($query->result() as  $value) {
                      $id=$value->id;
                  }

                  $id =$id+1;

                  $config['upload_path'] = './pictures/work/';
                  $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
                  $config['file_name'] = $id.'pics.jpg';
                  $config['overwrite']=true;
                  $this->load->library('upload', $config);
                  if($config['file_name']==''){

                  }else{
                       if ( ! $this->upload->do_upload())
                  {
                  echo 'error uploading file';
                  }else{
                      
                          $data['image']=$id.'pics.jpg';
                  }
                }

                  

                 
              $this->mdl_admin->_insert($data);
              redirect(base_url('admin/showPortfolio'));

      }

      
      function _in_you_go($email){
        $this->load->library('session');
          // give them a session variable and sent them to the admin panel
          $this->load->model('mdl_admin');
          $this->mdl_admin->tablename='adminusers';
          $query= $this->mdl_admin-> get_where_custom('email',$email);
          foreach ($query -> result() as $row){
            $user_id = $row->id;
            $name = $row->name;
            $email = $row->email;
          }
          $logged = 'logged';
          $this->session->set_userdata('user_id', $user_id);
           $this->session->set_userdata('logged', $logged);
          $this->session->set_userdata('name',$name);
          $this->session->set_userdata('email',$email);
          redirect (base_url ('admin/showAdminuser'));
        }


    function register(){
                   $this->load->library('session');
                  $this->load->model('mdl_admin');
                  $this->mdl_admin->tablename='adminusers';
                  $data= $this->get_form_data();
                  $unique = $this->mdl_admin->count_where('email', $data['email']);
                 // print_r($data['unique']);
                 // return;
                  if ($unique>=1) {
                     $regerror = '<div style="color:red; margin-bottom: 0px; padding: 0px; "" class="alert"> Email already used </div>';
                         $data['regerror'] = $regerror;
                          if($this->session->userdata('logged')){
                          $this->load->model('mdl_admin');      
                          $allAdminUsers = $this->mdl_admin->get_all_users('id'); 
                          $data['allAdminUsers'] = $allAdminUsers;
                          $this->load->view('adminuser', $data);
                        }else{
                          redirect(base_url('admin/login'));
                        }
                  }else{

                      $query=$this->mdl_admin->get('id');
    
                  $id=0;
                  foreach ($query->result() as  $value) {
                      $id=$value->id;
                  }

                  $id =$id+1;
                   if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != '') {
                   $config['upload_path'] = './pictures/admin/';
                  $config['allowed_types'] = 'jpg|png|jpeg|bmp|gif';
                  $config['file_name'] = $id.'pics.jpg';
                  $config['overwrite']=true;
                  $this->load->library('upload', $config);

                   if ( ! $this->upload->do_upload())
                  {
                  echo 'error uploading file';
                  }else{
                      
                          $data['picture']=$id.'pics_thumb.jpg';
                          $file_name = $id.'pics.jpg';
                          $filePath = './pictures/admin/';
                          $width = 320;
                          $height = 250;
                          $mainfolder ='pictures/admin';
                          $this->resize_image($mainfolder,$filePath,$file_name, $width, $height,$id);
                  }
                   }
                   else{
                  

                 }
            //  $p = $data['password'];
            //  $data['password'] = modules:: run('site_security/awesome_unbreakable_super_mega_hash',$p);
              $this->mdl_admin->_insert($data);
              redirect(base_url('admin/showAdminuser'));

                }
              
          

        
      }
            

       function deleteuser(){
       $filter = $this->uri->segment(3);
            $this->load->model('mdl_admin');
            $this->mdl_admin->tablename='adminusers';
             $query=$this->mdl_admin->get('id');
            $this->mdl_admin->_delete($filter);
            redirect(base_url('admin/showAdminuser'));

  }
  function deleteportfolio(){
         $filter = $this->uri->segment(3);
            $this->load->model('mdl_admin');
            $this->mdl_admin->tablename='portfolio';
             $query=$this->mdl_admin->get('id');
            $this->mdl_admin->_delete($filter);
            redirect(base_url('admin/showPortfolio'));
  }

       function deleteinqiury($filter){
       
            $this->load->model('mdl_admin');
            $this->mdl_admin->tablename='inqiury';
             $query=$this->mdl_admin->get('id');
            $id=0;
        foreach ($query->result() as  $value) {
         
           $inqiury_id = $value->id;
         }

          $id =$id+1;
            $this->mdl_admin->_delete($filter);
            redirect(base_url('admin/inqiury'));

  }

  function deleteWork($filter){
     $this->load->model('mdl_admin');
            $this->mdl_admin->tablename='work';
             $query=$this->mdl_admin->get('id');
            $id=0;
        foreach ($query->result() as  $value) {
         
           $user_id = $value->user_id;
         }

          $id =$id+1;
            $this->mdl_admin->_delete($filter);
            redirect(base_url('admin/work'));

  }

      public function resize_image($mainfolder,$filePath,$file_name, $width, $height,$id) {
                    $this->load->library('image_lib');
                    $config['file_name'] = $file_name;
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $filePath.$file_name;
                    
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = FALSE;
                    $config['width']    = $width;
                    $config['height']   = $height;
                    $config['new_image'] = './'.$mainfolder.'/thumb/';

                    $this->image_lib->initialize($config);
                    $this->load->library('image_lib', $config); 
                     if(!$this->image_lib->resize())
                    { 
                        echo $this->image_lib->display_errors();
                    }   

                }


  
       function pword_check($password) {
                $email = $this->input->post('email',TRUE);
                //the next line helps remove the encryption property
                $password = modules::run('site_security/make_hash', $password);
                $this->load->model('mdl_admin');
                $this->mdl_admin->tablename='adminusers';
                // collect username and password and send them to fuuction pword_check in mdl_user
                $result = $this->mdl_admin->pword_check($email, $password);

                      if ($result == FALSE)
                      {
                        // ensure u add form validation php file to libraries and run this
                        // or u can search for hmvc call back on youtube 
                              $this->form_validation->set_message('pword_check', 'You did not enter correct username and/or password ');
                              return FALSE;
                      }
                      else
                      {
                              return TRUE;
                      }
              }


    

      function get_form_data(){
          $data = $this->input->post();
          return $data;
      }

       function logout(){
             $this->load->library('session');
            $this->session->sess_destroy();
            redirect('admin/index');
          }
          function showLogin($data){
            if($data['errormsg'] == ''){
              $this->load->view('login');
              
            }
            else{
              $this->load->view('login', $data);
            }
          }

    
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

?>